extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_LB1_pressed():
	pass # Replace with function body.
	$Story1.hide()
	$Characters1.hide()
	$Characters2.show()
	$Story2.show()
	$Button1.hide()
	$Button2.show()

func _on_LB2_pressed():
	$Story2.hide()
	$Characters2.hide()
	$Characters3.show()
	$Story3.show()
	$Button2.hide()
	$Button3.show() # Replace with function body.

func _on_LB3_pressed():
	get_tree().change_scene(str("res://Scenes/HowToPlay.tscn")) # Replace with function body.
