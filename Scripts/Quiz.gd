extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_LinkButton_pressed():
	$"Quiz-1".hide()
	$B1.hide()
	$"Quiz-2".show()
	$B2.show() # Replace with function body.

func _on_LinkButton2_pressed():
	$"Quiz-2".hide()
	$B2.hide()
	$"Quiz-3".show()
	$B3.show() # Replace with function body.


func _on_LinkButton3_pressed():
	$"Quiz-3".hide()
	$B3.hide()
	$"Quiz-4".show()
	$Options.show() # Replace with function body.

func _on_Wrong_pressed():
	$Options.hide()
	$"Quiz-4".hide()
	$"Quiz-5b".show()
	$B5b.show() # Replace with function body.

func _on_Right_pressed():
	$Options.hide()
	$"Quiz-4".hide()
	$"Quiz-5a".show()
	$B5a.show() # Replace with function body.

func _on_LinkButton5a_pressed():
	$"Quiz-5a".hide()
	$B5a.hide()
	$"Quiz-6".show()
	$B6.show() # Replace with function body.

func _on_LinkButton5b_pressed():
	$"Quiz-5b".hide()
	$B5b.hide()
	get_tree().change_scene(str("res://Scenes/Menu.tscn"))# Replace with function body.

func _on_LinkButton6_pressed():
	$"Quiz-6".hide()
	$B6.hide()
	get_tree().change_scene(str("res://Scenes/Level 2.tscn")) # Replace with function body.
