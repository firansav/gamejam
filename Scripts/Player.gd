extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600
export (int) var dash_speed = 600

const UP = Vector2(0,-1)

var velocity = Vector2()
var has_double_jumped = false
var on_ground = false

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func get_input():
	velocity.x = 0
	
	if is_on_floor():
		on_ground = true
		$AnimatedSprite.play("idle")
	else:
		on_ground = false
	
	if Input.is_action_just_pressed('jump') or Input.is_action_just_pressed('ui_up'):
		if on_ground == true:
	        velocity.y = jump_speed
	        has_double_jumped = false
	        on_ground = false
		elif on_ground == false and has_double_jumped == false:
	        velocity.y = jump_speed
	        has_double_jumped = true
		$AnimatedSprite.play("jump")
		
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
		$AnimatedSprite.flip_h = false
		$AnimatedSprite.play("walk")
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		$AnimatedSprite.flip_h = true
		$AnimatedSprite.play("walk")
	
	if Input.is_action_pressed('ui_right') && Input.is_action_pressed('ui_select'):
		velocity.x += dash_speed
		$AnimatedSprite.flip_h = false
	if Input.is_action_pressed('ui_left') && Input.is_action_pressed('ui_select'):
		velocity.x -= dash_speed
		$AnimatedSprite.flip_h = true
		
func _physics_process(delta):
    velocity.y += delta * GRAVITY
    get_input()
    velocity = move_and_slide(velocity, UP)

func teleport_to(target_position):
	position = target_position
