extends RichTextLabel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.set_wait_time(global.time)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	set_text(str(int($Timer.get_time_left())))

func _on_Timer_timeout():
	get_tree().change_scene(str("res://Scenes/GameOver.tscn")) # Replace with function body.
