extends Node

export(String) var scene_to_load

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_LinkButton_pressed():
	global.time = 120
	get_tree().change_scene(str("res://Scenes/Story.tscn"))

func _on_LinkButton2_pressed():
	get_tree().quit()

func _on_Door_body_entered(body):
	if body.get_name() == "Player":
		get_tree().change_scene(str("res://Scenes/"+ scene_to_load +".tscn")) # Replace with function body.
