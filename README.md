# Stop Brother!
This game is made to finish Game Jam in Game Development's course.

Find this game on [itch.io](https://firansav.itch.io/stop-brother) for visualization.

## Story
Danny is the youngest in the family, and he has an older brother named Manny. Originally, Danny and Manny are best buddies, they don't fight at all. However, one day, Danny and Manny are fighting over something to the point Manny feels upset with the younger. Manny then decides to move out of house from the time being. On the day of Manny's supposed departure, Danny found out about Manny's plan and immediately feels guilty. Danny then goes to the airport to stop Manny from leaving.

## About Game

There's 2 Level in this game. Each level's objective is to find the door within the time limit. Before advancing to the next level, player has to answer a question related to the game. If the answer is correct, player's timer will be renewed and they can proceed to the next level. Hints to the door are scattered throughout the levels.

## How to Play

 - Arrow Keys (Up to jump, Left to walk to the left direction, Right to
   walk to the right direction)
 - Space Bar + Arrow Key to Dash
